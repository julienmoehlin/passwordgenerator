# Password Generator

![alt text](images/pwGeneratorGui.gif)

### Aim

Provide a random password.

### About Password Generator

The user can choose:

- Several types of characters:

	- Lowercase characters
		>`abcdefghijklmnopqrstuvwxyz`

	- Uppercase characters
		>`ABCDEFGHIJKLMOPQRSTUVWXYZ`

	- Numbers
		>`0123456789`

	- Special characters 
		>`,?;.><:/=+-_^%$*()}[]{|\'\"&!°'`

- The length of password

- Copy the generated password