__author__ = 'Julien MOEHLIN'

import random
from string import ascii_lowercase
from string import ascii_uppercase
from string import digits
from tkinter import *
import tkinter as tk
from tkinter import ttk
import webbrowser

def password_generator():
	"""
	Generate password
	password_generator() need a lenght and at least one set of characters
	"""
	list_string = []
	password = ""
	if varNumber.get() == 1:
		list_string.append(digits)
	if varSpecial.get() == 1:
		list_string.append(',?;.><:/=+-_^%$*()}[]{|\'\"&!°')
	if varLowerChar.get() == 1:
		list_string.append(ascii_lowercase)
	if varUpperChar.get() == 1:
		list_string.append(ascii_uppercase)
	if varNumber.get() + varSpecial.get() + varLowerChar.get() + varUpperChar.get() == 0:
		scvalue.set("Error : Select at least one set.")
		screen.update()
	else:
		if length_pw.get().isdigit():
			lenPW = int(length_pw.get())
			for i in range(lenPW):
				password += random.choice(random.choice(list_string))
			scvalue.set(password)
			screen.update()
		else:
			scvalue.set("Error : Not a number (int) as input.")
			screen.update()

def copy():
	"""
	Copy password generated on clipboard
	"""
	tk.clipboard_clear()
	tk.clipboard_append(scvalue.get())
	tk.update()

if __name__ == '__main__':	
	tk = Tk()
	tk.title("Password Generator")
	tk.minsize(420, 280)
	tk.maxsize(420, 280)
	titleText = Label(tk, text=" Password Generator ", font ="Arial 20 bold", borderwidth=4, relief="raised")
	titleText.pack(padx = "0", pady = "5")
	frameCheckBoxUp = Frame(tk)
	frameCheckBoxDown = Frame(tk)
	varNumber = IntVar()
	varNumber.set(1)
	number = ttk.Checkbutton(frameCheckBoxUp, text="Numbers", variable=varNumber)
	varSpecial = IntVar()
	varSpecial.set(1)
	specialCharacter = ttk.Checkbutton(frameCheckBoxDown, text="Special Characters", variable=varSpecial)
	varLowerChar = IntVar()
	varLowerChar.set(1)
	lowerChar = ttk.Checkbutton(frameCheckBoxUp, text="Lowercase Characters", variable=varLowerChar)
	lowerChar.pack(padx = "20",side="left")
	varUpperChar = IntVar()
	varUpperChar.set(1)
	upperChar = ttk.Checkbutton(frameCheckBoxDown, text="Uppercase Characters", variable=varUpperChar)
	upperChar.pack(padx = "20", side="left")
	specialCharacter.pack(padx = "20", side="left")
	number.pack(padx = "20", side="left")
	frameCheckBoxUp.pack(fill=X, pady = "5")
	frameCheckBoxDown.pack(fill=X, pady = "5")
	frame = Frame(tk)
	text = Label(frame, text="Length :", font ="Lucida 15", fg="black")
	text.pack(side="left", padx = "20")
	length_pw = StringVar()
	length = Entry(frame, textvar=length_pw, width=20)
	length.bind("<Return>", (lambda event: password_generator()))
	length.pack(side="left")
	frame.pack(fill=X, pady="5")
	frameButton = Frame(tk)
	button = ttk.Button(frameButton, text="Generate", command=password_generator)
	button.pack()
	frameButton.pack(pady="10")
	scvalue = StringVar()
	scvalue.set('')
	frameGeneratePw = Frame(tk)
	screen = Entry(frameGeneratePw, textvar=scvalue, width=30)
	screen.pack(side="left", padx = "20")
	button2 = ttk.Button(frameGeneratePw, text="Copy", command=copy)
	button2.pack(side="left")
	frameGeneratePw.pack(fill=X, pady="5")
	linkGitlab = Label(tk, text="Gitlab project", fg="forestgreen", font= "Verdana 10")
	linkGitlab.bind("<Button-1>", lambda event : webbrowser.open_new("https://gitlab.com/julienmoehlin/passwordgenerator"))
	linkGitlab.bind("<Enter>",  lambda event : linkGitlab.configure(font = "Verdana 10 underline", fg="red"))
	linkGitlab.bind("<Leave>", lambda event : linkGitlab.configure(font = "Verdana 10", fg="forestgreen"))
	linkGitlab.pack(pady="10", side=RIGHT, padx=50)
	linkGithub = Label(tk, text="Github", fg="forestgreen", font= "Verdana 10")
	linkGithub.bind("<Button-1>", lambda event : webbrowser.open_new("https://github.com/JulienMoehlin"))
	linkGithub.bind("<Enter>",  lambda event : linkGithub.configure(font = "Verdana 10 underline", fg="red"))
	linkGithub.bind("<Leave>", lambda event : linkGithub.configure(font = "Verdana 10", fg="forestgreen"))
	linkGithub.pack(pady="10", side=LEFT, padx=50)
	tk.mainloop()